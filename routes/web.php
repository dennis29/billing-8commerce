<?php  

Route::get('/', 'IndexController@index');
Route::get('/pdf-to-text-2', 'IndexController@extractPDF2');
Route::get('/pdf-to-text', 'IndexController@extractPDF');
Route::get('/cut-text', 'IndexController@potongString');
Route::get('page-not-found',['as' => 'pagenotfound','uses' => 'IndexController@pageNotFound']);
Route::get('server-error',['as' => 'servererror','uses' => 'IndexController@serverError']);
Route::get('/forgot-password', 'IndexController@forgotPassword');

// change language
Route::get('lang/{locale}', 'HomeController@lang');


Route::get('/coba-ektract-invoice', 'JobController@extractFilesData');


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/dashboard-company', 'HomeController@dashboardCompany')->name('dashboardCompany');
Route::resource('/users', 'UserController');
Route::get('/users/listuser', 'UserController@listUsers');
Route::get('/user/get-profile', 'UserController@getProfile');
Route::post('/user/change-password', 'UserController@changePassword');
Route::get('/user/get-super-admin', 'UserController@getSuperAdmin');
Route::delete('/user/delete-all/{id}', 'UserController@deleteAll'); 


Route::resource('/roles', 'RoleController');
Route::delete('/role/delete-all/{id}', 'RoleController@deleteAll');
Route::get('/get-role', 'RoleController@getRole');

Route::get('/getService', 'UltimateController@getService');
Route::get('/searchAreaCode', 'UltimateController@searchAreaCode');

Route::resource('/couriers', 'CourierController');
Route::delete('/courier/delete-all/{id}', 'CourierController@deleteAll');
Route::get('/get-courier', 'CourierController@getCourier');
Route::get('/get-courier-tracking-awb', 'CourierController@getCourierForTracking');

Route::resource('/customers', 'CustomerController');
Route::delete('/customers/delete-all/{id}', 'CustomerController@deleteAll');
Route::get('/get-customers', 'CustomerController@getCustomer');

Route::resource('/areas', 'MAreaController');
Route::delete('/areas/delete-all/{id}', 'MAreaController@deleteAll');
Route::get('/get-area', 'MAreaController@getArea');
Route::get('/getAreas', 'MAreaController@getDataWithCodeArea');

Route::resource('/pricesFor8Commerce', 'PriceController');
Route::post('/pricesFor8Commerce/storePrice', 'PriceController@storePrice');
Route::delete('/pricesFor8Commerce/delete-all/{id}', 'PriceController@deleteAll');
Route::get('/get-price', 'PriceController@getPrice');
Route::get('/getAreaCode', 'PriceController@getAreaCode');
Route::get('/getAllPrices', 'PriceController@getDataWithCodeArea');

Route::resource('/pricesForCustomer', 'PriceByCustomerController');
Route::post('/pricesForCustomer/storePrice', 'PriceByCustomerController@storePrice');
Route::delete('/pricesForCustomer/delete-all/{id}', 'PriceByCustomerController@deleteAll');
Route::get('/getAllPricesByCustomer', 'PriceByCustomerController@getDataWithCodeArea');

Route::resource('/pricesForPublish', 'PriceByPublishController');
Route::post('/pricesForPublish/storePrice', 'PriceByPublishController@storePrice');
Route::delete('/pricesForPublish/delete-all/{id}', 'PriceByPublishController@deleteAll');
Route::get('/getAllPricesForPublish', 'PriceByPublishController@getDataWithCodeArea');

Route::resource('/multiDestPriceFor8com', 'MultiDestPriceFor8comController');
Route::post('/multiDestPriceFor8com/storePrice', 'MultiDestPriceFor8comController@storePrice');
Route::delete('/multiDestPriceFor8com/delete-all/{id}', 'MultiDestPriceFor8comController@deleteAll');
Route::get('/getAllMultiDestPricesBy8com', 'MultiDestPriceFor8comController@getDataWithCodeArea');

Route::resource('/multiDestPriceForCus', 'MultiDestPriceForCusController');
Route::post('/multiDestPriceForCus/storePrice', 'MultiDestPriceForCusController@storePrice');
Route::delete('/multiDestPriceForCus/delete-all/{id}', 'MultiDestPriceForCusController@deleteAll');
Route::get('/getAllMultiDestPricesByCustomer', 'MultiDestPriceForCusController@getDataWithCodeArea');

Route::resource('/Generaldiscounts', 'KondisiDiskonController');
Route::post('/Generaldiscounts/storeDiscount', 'KondisiDiskonController@storeDiscount');
Route::delete('/Generaldiscounts/delete-all/{id}', 'KondisiDiskonController@deleteAll');
Route::get('/get-discount', 'KondisiDiskonController@getDiscount');
Route::get('/combineAreaCode', 'KondisiDiskonController@combineAreaCode');

Route::resource('/Rangediscounts', 'DiscountByRangeController');
Route::post('/Rangediscounts/storeDiscount', 'DiscountByRangeController@storeDiscount');
Route::delete('/Rangediscounts/delete-all/{id}', 'DiscountByRangeController@deleteAll');

Route::resource('/Customerdiscounts', 'DiscountByCustomerController');
Route::post('/Customerdiscounts/storeDiscount', 'DiscountByCustomerController@storeDiscount');
Route::delete('/Customerdiscounts/delete-all/{id}', 'DiscountByCustomerController@deleteAll');

Route::get('/getBody', 'UrlApiTrackingController@getBody');
Route::post('/getDetailForTrackingAWB', 'UrlApiTrackingController@getDetailForTrackingAWB');


Route::resource('/invoices', 'InvoiceController');  
Route::get('/get-invoice/{id}', 'InvoiceController@getById'); 
Route::post('/invoices-edit', 'InvoiceController@update');  
Route::post('/download-invoice-file', 'InvoiceController@downloadFile');  


Route::get('/get-invoice-detail/{id}/by-id', 'InvoiceDetailController@index');
Route::post('/download-invoice-file-details', 'InvoiceDetailController@downloadData');
Route::post('/download-invoice-file-details-by-company', 'InvoiceDetailController@downloadDataByCompany');

Route::get('/get-order/{id}', 'OrderController@detail');
Route::get('/get-order/{id}/{company}/by-company', 'OrderController@getByCompany');


Route::post('/invoices-process', 'JobController@extractFiles'); 


Route::resource('/invoices-templete', 'InvoiceTempleteController');  
Route::post('/invoices-templete-download-file', 'InvoiceTempleteController@downloadFile');  

Route::get('/CekResi-view', 'ViewController@index');
Route::get('/Customerdiscount-view', 'ViewController@index');
Route::get('/Generaldiscount-view', 'ViewController@index');
Route::get('/Rangediscount-view', 'ViewController@index');
Route::get('/pricesForCustomer-view', 'ViewController@index');
Route::get('/pricesFor8Commerce-view', 'ViewController@index');
Route::get('/pricesForPublish-view', 'ViewController@index');
Route::get('/multiDestPriceFor8com-view', 'ViewController@index');
Route::get('/multiDestPriceForCus-view', 'ViewController@index');
Route::get('/areas-view', 'ViewController@index');
Route::get('/users-view', 'ViewController@index');
Route::get('/roles-view', 'ViewController@index');
Route::get('/couriers-view', 'ViewController@index');
Route::get('/customers-view', 'ViewController@index');
Route::get('/invoice-view', 'ViewController@index');

Route::get('/get-algoritma', 'AlgoritmaController@getAlgoritma');