<?php  

Route::get('/', 'IndexController@index');
Route::get('/pdf-to-text-2', 'IndexController@extractPDF2');
Route::get('/pdf-to-text', 'IndexController@extractPDF');
Route::get('/cut-text', 'IndexController@potongString');
Route::get('page-not-found',['as' => 'pagenotfound','uses' => 'IndexController@pageNotFound']);
Route::get('server-error',['as' => 'servererror','uses' => 'IndexController@serverError']);
Route::get('/forgot-password', 'IndexController@forgotPassword');

// change language
Route::get('lang/{locale}', 'HomeController@lang');


Route::get('/coba-ektract-invoice', 'JobController@extractFilesData');


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/dashboard-company', 'HomeController@dashboardCompany')->name('dashboardCompany');
Route::resource('/users', 'UserController');
Route::get('/users/listuser', 'UserController@listUsers');
Route::get('/user/get-profile', 'UserController@getProfile');
Route::post('/user/change-password', 'UserController@changePassword');
Route::get('/user/get-super-admin', 'UserController@getSuperAdmin');
Route::delete('/user/delete-all/{id}', 'UserController@deleteAll'); 


Route::resource('/roles', 'RoleController');
Route::delete('/role/delete-all/{id}', 'RoleController@deleteAll');
Route::get('/get-role', 'RoleController@getRole');

Route::resource('/couriers', 'CourierController');
Route::delete('/courier/delete-all/{id}', 'CourierController@deleteAll');
Route::get('/get-courier', 'CourierController@getCourier');

Route::resource('/areas', 'MAreaController');
Route::delete('/areas/delete-all/{id}', 'MAreaController@deleteAll');
Route::get('/get-area', 'MAreaController@getArea');
Route::get('/getAreas', 'MAreaController@getAreas');

Route::resource('/prices', 'PriceController');
Route::delete('/prices/delete-all/{id}', 'PriceController@deleteAll');
Route::get('/get-price', 'PriceController@getPrice');
Route::get('/getAreaCode', 'PriceController@getAreaCode');
Route::get('/getAllPrices', 'PriceController@getAllPrices');

Route::resource('/discounts', 'KondisiDiskonController');
Route::delete('/discounts/delete-all/{id}', 'KondisiDiskonController@deleteAll');
Route::get('/loadingDataDiscount', 'KondisiDiskonController@loadingDataDiscount');
Route::get('/get-discount', 'KondisiDiskonController@getDiscount');
Route::get('/combineAreaCode', 'KondisiDiskonController@combineAreaCode');
Route::post('/SaveDataDiscount', 'KondisiDiskonController@SaveDataDiscount');
Route::put('/UpdateDataDiskon/{id}', 'KondisiDiskonController@UpdateDataDiskon');

Route::resource('/invoices', 'InvoiceController');  
Route::get('/get-invoice/{id}', 'InvoiceController@getById'); 
Route::post('/invoices-edit', 'InvoiceController@update');  
Route::post('/download-invoice-file', 'InvoiceController@downloadFile');  


Route::get('/get-invoice-detail/{id}/by-id', 'InvoiceDetailController@index');
Route::post('/download-invoice-file-details', 'InvoiceDetailController@downloadData');
Route::post('/download-invoice-file-details-by-company', 'InvoiceDetailController@downloadDataByCompany');

Route::get('/get-order/{id}', 'OrderController@detail');
Route::get('/get-order/{id}/{company}/by-company', 'OrderController@getByCompany');


Route::post('/invoices-process', 'JobController@extractFiles'); 


Route::resource('/invoices-templete', 'InvoiceTempleteController');  
Route::post('/invoices-templete-download-file', 'InvoiceTempleteController@downloadFile');  

Route::get('/discounts-view', 'ViewController@index');
Route::get('/prices-view', 'ViewController@index');
Route::get('/areas-view', 'ViewController@index');
Route::get('/users-view', 'ViewController@index');
Route::get('/roles-view', 'ViewController@index');
Route::get('/couriers-view', 'ViewController@index');
Route::get('/invoice-view', 'ViewController@index');

Route::get('/get-algoritma', 'AlgoritmaController@getAlgoritma');