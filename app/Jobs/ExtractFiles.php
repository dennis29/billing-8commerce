<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable; 

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\algoritma;
use App\Models\Order;
use App\Models\OrderDetail;

class ExtractFiles implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $invoice = null;
    private $url = 'https://oms.8commerce.com/oms/index.php?r=apiRekon/getOrderByAwb';
	private $arr = array();
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice)
    {
		ini_set('max_execution_time', 600);// 0 = Unlimited
	    $this->invoice = $invoice;
	    $Invoice = new Invoice();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {  
        ini_set('max_execution_time', 600);// 0 = Unlimited
        $path = public_path().'/invoice/'.$this->invoice->file_name; 
        $rows = [];

        if($this->invoice->courier->courier_code == 'tiki'){
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
            $worksheet = $spreadsheet->setActiveSheetIndex(0)->rangeToArray('A1:M1000'); 
            foreach ($worksheet AS $row) {
                if($row[0] == "No") continue;
                
                $cek = $this->checkNumber($row[0]);
                if($cek == true){
                    $rows[] = $row; 
                }
            } 
        }elseif($this->invoice->courier->courier_code == 'adex'){
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
            $worksheet = $spreadsheet->setActiveSheetIndex(0)->rangeToArray('A1:R1000'); 
            foreach ($worksheet AS $row) {
                if($row[0] == "No") continue;
                
                $cek = $this->checkNumber($row[0]);
                if($cek == true){
                    $rows[] = $row; 
                }
            } 

        }elseif($this->invoice->courier->courier_code == 'jne'){
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
            $worksheet = $spreadsheet->setActiveSheetIndex(0)->rangeToArray('A1:L1000'); 
            foreach ($worksheet AS $row) { 
                $cek = $this->checkNumber($row[0]);
                if($cek == true){
                    $rows[] = $row; 
                }
            } 

        }else{
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
            $worksheet = $spreadsheet->getActiveSheet();
            foreach ($worksheet->getRowIterator() AS $row) {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(TRUE); // This loops through all cells,
                $cells = [];
                foreach ($cellIterator as $cell) {
                    $cells[] = $cell->getValue();
                }
                $rows[] = $cells;
            }

        }

        $this->checkCourier($this->invoice->courier->courier_code,$rows);
       
    }

    private function checkCourier($courier,$array){
        if($courier == 'sap'){
            $this->courierSAP($array); 
        }elseif($courier == 'j&t'){
            $this->courierJET($array); 
        }elseif($courier == 'tiki'){
            $this->courierTIKI($array); 
        }elseif($courier == 'wahana'){
            $this->courierWAHANA($array); 
        }elseif($courier == 'adex'){
            $this->courierADEX($array); 
        }elseif($courier == 'jne'){
            return $this->courierJNE($array); 
        }else{
            Invoice::where('id',$this->invoice->id)->update(array('status' => 'error', 'remarks' => 'courier code not found in backend', 'data' => json_encode($array)));
        } 

    }

    private function getDataFromSci(){
        $file = InvoiceDetail::where('invoice_files_id', $this->invoice->id)->orderBy('id','ASC')->get(); 

        $client = new Client(['verify' => false]);

        foreach($file as $f){
            $request = $client->get($this->url.'&token=_cilukba_&awb='.$f->awb_no);
            $response = $request->getBody()->getContents();
            $order = json_decode($response,TRUE);
            if($order['status'] == 'success'){
                $orderHeader =  $this->saveOrder($this->invoice->id, $f->id, $order['data']);
                $orderDetail =  $this->saveOrderDetail($this->invoice->id, $f->id,$orderHeader, $order['data']);
            }else{
                InvoiceDetail::where('id',$f->id)->update(array('status' => 'false'));
            }
        }
		Invoice::where('id',$this->invoice->id)->update(array('status' => 'complete'));
		$this->get_potongan_per_detail();
		die();
    }

	private function get_potongan_per_detail(){
		ini_set('max_execution_time', 600);// 0 = Unlimited
		$data = $this->invoice->getDataInvoiceDetail($this->invoice->id);
        $courier_code = $this->invoice->getCourierCode($this->invoice->courier_id);
        $client = new Client(['verify' => false]);
		foreach($data as $k=>$v){

            $harga_publish = $this->invoice->getHargaPublish($v->id,$v->courier_id,$v->company_id,$v->date_invoice);

                if($harga_publish){
                    if($harga_publish[0]->publishPriceByDestination)
                    {
                        $array['publishPriceByDestination'] = $harga_publish[0]->publishPriceByDestination;
                        $array['publishPriceByWeight'] = $harga_publish[0]->publishPriceByDestination*$v->weight_total;
                    }
                    if($harga_publish[0]->commercePriceByDestination)
                    {
                        $array['commercePriceByDestination'] = $harga_publish[0]->commercePriceByDestination;
                        $array['commercePriceByWeight'] = $harga_publish[0]->commercePriceByDestination*$v->weight_total;
                    }
                    if($harga_publish[0]->customerPriceByDestination)
                    {
                        $array['customerPriceByDestination'] = $harga_publish[0]->customerPriceByDestination;
                        $array['customerPriceByWeight'] = $harga_publish[0]->customerPriceByDestination*$v->weight_total;
                    }
                }
			
			$array['commercePriceBy3Percent'] = $v->price*3/100;
			$array['commercePriceBy5Percent'] = $v->price*5/100;
			$array['commercePriceBy10Percent'] = $v->price*10/100;
			$array['commercePriceByRange'] = null;
			$array['customerPriceByDiscount'] = null;
			
			$get_data = $this->invoice->getAllDiscount($v->courier_id,$v->price,$v->company_id,$v->date_invoice);
			
			if($get_data){
				if($get_data[0]->commercePriceByRange)
				{
					$array['commercePriceBy3Percent'] = null;
					$array['commercePriceBy5Percent'] = null;
					$array['commercePriceBy10Percent'] = null;
					$array['commercePriceByRange'] = $get_data[0]->commercePriceByRange;
				}
				
				if($get_data[0]->customerPriceByDiscount)
				{
					$array['customerPriceByDiscount'] = $get_data[0]->customerPriceByDiscount;
				}
				
			}
		
			InvoiceDetail::where('id',$v->id)->update($array);
			
		}
	}

    
    private function courierJNE($array){
        ini_set('max_execution_time', 600);// 0 = Unlimited
        $x=0;
        $res = [];
		
        for($x; $x < count($array);$x++){ 
	
            InvoiceDetail::create(['invoice_files_id'=>$this->invoice->id, 'customer_code' => '', 'status' => 'true', 'service' =>$array[$x][11], 'awb_no' => $array[$x][2] ,'qty' => (int)$array[$x][5], 'weight_total' => (int)$array[$x][6], 'bag_total' => 0, 'cod_amount' => 0, 'insurance_amount' => (int)$this->rupiahToInt($array[$x][8]) , 'weight_price' => 0, 'other_amount' => (int)$this->rupiahToInt($array[$x][9]),'price' =>(int)$this->rupiahToInt($array[$x][7])]);             
        } 

        $this->getDataFromSci(); 

    }

    private function courierADEX($array){
        ini_set('max_execution_time', 600);// 0 = Unlimited
        $x=0;
        $res = [];
        for($x; $x < count($array);$x++){ 
			
            InvoiceDetail::create(['invoice_files_id'=>$this->invoice->id, 'customer_code' => '', 'status' => 'true', 'service' =>'', 'awb_no' => $array[$x][2] ,'qty' => 0, 'weight_total' => (int)$array[$x][9], 'bag_total' => (int)$array[$x][8], 'cod_amount' => 0, 'insurance_amount' => 0 , 'weight_price' => (int)$this->rupiahToInt($array[$x][14]), 'other_amount' => (int)$this->rupiahToInt($array[$x][15]),'price' => (int)$this->rupiahToInt($array[$x][16])]);             
        } 

        $this->getDataFromSci();
    }
    
    private function courierWAHANA($array){
        ini_set('max_execution_time', 600);// 0 = Unlimited
        $x=2;
        $res = [];
        for($x; $x < count($array);$x++){
            $cek = $this->checkNumber($array[$x][1]);
            if($cek == false) break;
			
            InvoiceDetail::create(['invoice_files_id'=>$this->invoice->id, 'customer_code' => '', 'status' => 'true', 'service' =>'', 'awb_no' => $array[$x][2] ,'qty' => 0, 'weight_total' => (int)$array[$x][10], 'bag_total' => (int)$array[$x][9], 'cod_amount' => 0, 'insurance_amount' => 0 , 'weight_price' => (int)$array[$x][16] , 'other_amount' => 0,'price' => (int)$array[$x][17]]);             
        } 

        $this->getDataFromSci();

    }

    private function courierTIKI($array){
        ini_set('max_execution_time', 600);// 0 = Unlimited 
        $x=0; 
        for($x; $x < count($array);$x++){

            InvoiceDetail::create(['invoice_files_id'=>$this->invoice->id, 'customer_code' => '', 'status' => 'true', 'service' =>'', 'awb_no' => $array[$x][10] ,'qty' => 0, 'weight_total' => (int)$array[$x][11], 'bag_total' => 0, 'cod_amount' => 0, 'insurance_amount' => 0 , 'weight_price' => 0, 'other_amount' =>0 ,'price' => (int)$this->rupiahToInt($array[$x][12])]);             
        } 

        $this->getDataFromSci();

    }

    private function courierJET($array){
        ini_set('max_execution_time', 600);// 0 = Unlimited
        $x=12;
        $res = [];

        //print_r($array);die();
        //echo json_encode($array);die();
        for($x; $x < count($array);$x++){
            $cek = $this->checkNumber($array[$x][1]);
            if($cek == false) break;
			
            InvoiceDetail::create(['invoice_files_id'=>$this->invoice->id, 'customer_code' => '', 'status' => 'true', 'service' =>'', 'awb_no' => $array[$x][1] ,'qty' => 0, 'weight_total' => (int)$array[$x][6], 'bag_total' => 0, 'cod_amount' => 0, 'insurance_amount' => (int)$array[$x][7] , 'weight_price' => 0, 'other_amount' =>0 ,'price' => (int)$array[$x][9]]);             
        } 
		
        $this->getDataFromSci();
    }

    private function courierSAP($array){
        ini_set('max_execution_time', 600);// 0 = Unlimited
        $x=8;
        $res = [];
        for($x; $x < count($array);$x++){
            if($array[$x][1] == '') break;

            $other_amount = $this->otherAmount('sap',$array[$x]);
						
            InvoiceDetail::create(['invoice_files_id'=>$this->invoice->id, 'customer_code' => $array[$x][4], 'status' => 'true', 'service' => $array[$x][9], 'awb_no' => $array[$x][1] ,'qty' => 0, 'weight_total' => (int)$this->IsNullOrEmptyString($array[$x][20]), 'bag_total' => (int)$this->IsNullOrEmptyString($array[$x][21]), 'cod_amount' => (int)$this->IsNullOrEmptyString($array[$x][18]), 'insurance_amount' => (int)$this->IsNullOrEmptyString($array[$x][45]) , 'weight_price' => 0 , 'other_amount' => (int)$other_amount ,'price' => (int)$this->IsNullOrEmptyString($array[$x][48]),'8comm_price' =>(int)$this->IsNullOrEmptyString($array[$x][48])*$this->arr['pot_8commerce'],'customer_price' =>(int)$this->IsNullOrEmptyString($array[$x][48])-$potongan]);         
        }
        $this->getDataFromSci(); 
    }

    private function otherAmount($courier,$array){
        if($courier == 'sap'){
            $pulus = $this->IsNullOrEmptyString($array[44]) + $this->IsNullOrEmptyString($array[46]) + $this->IsNullOrEmptyString($array[47]);
            return $pulus;
        }

        return false;
    }

    private function checkNumber($no) {  
      if(is_numeric($no)){
         return true;
        }else{
        return false;
        }
    }

    private function IsNullOrEmptyString($str){
        if(!isset($str) || trim($str) === ''){
            return 0;
        }else{
            return $str;
        }
    }

    private function rupiahToInt($value){
        return preg_replace("/[^0-9]/", "", $value);
    }

    private function saveOrder($invoiceId, $invoiceDetailId, $array){
 
            $masukHeader = array(
                'invoice_files_id'          => $invoiceId,
                'invoice_file_details_id'   => $invoiceDetailId,
                'order_header_id'           => $array['header']['order_header_id'],
                'company_id'                => $array['company']['company_id'],
                'company_name'              => $array['company']['name'],
                'order_no'                  => $array['header']['order_no'],
                'order_date'                => $array['header']['order_date'],
                'due_date'                  => $array['header']['due_date'],
                'status'                    => $array['header']['status'],
                'courier_id'                => $array['header']['courier_id'],
                'special_packaging'         => $array['header']['special_packaging'],
                'awb_no'                    => $array['header']['awb_no'],
                'insured'                   => $array['header']['insured'],
                'insured_by_id'             => $array['header']['insured_by_id'],
                'cod'                       => $array['header']['cod'],
                'payment_status'            => $array['header']['payment_status'],
                'create_by'                 => $array['header']['create_by'],
                'dest_name'                 => $array['header']['dest_name'],
                'dest_addddress1'           => $array['header']['dest_address1'],
                'dest_address2'             => $array['header']['dest_address2'],
                'dest_province'             => $array['header']['dest_province'],
                'dest_city'                 => $array['header']['dest_city'],
                'dest_area'                 => $array['header']['dest_area'],
                'dest_sub_area'             => $array['header']['dest_sub_area'],
                'dest_postal_code'          => $array['header']['dest_postal_code'],
                'dest_village'              => $array['header']['dest_village'],
                'dest_remarks'              => $array['header']['dest_remarks'],
                'ori_name'                  => $array['header']['ori_name'],
                'ori_address1'              => $array['header']['ori_address1'],
                'ori_address2'              => $array['header']['ori_address2'],
                'ori_province'              => $array['header']['ori_province'],
                'ori_city'                  => $array['header']['ori_city'],
                'ori_area'                  => $array['header']['ori_area'],
                'ori_sub_area'              => $array['header']['ori_sub_area'],
                'ori_postal_code'           => $array['header']['ori_postal_code'],
                'ori_village'               => $array['header']['ori_village'],
                'ori_remarks'               => $array['header']['ori_remarks'],
                'fulfillment_center_id'     => $array['header']['fulfillment_center_id'],
                'order_source'              => $array['header']['order_source'],
                'create_time'               => $array['header']['create_time'],
                'update_time'               => $array['header']['update_time'],
                'dest_phone'                => $array['header']['dest_phone'],
                'dest_mobile'               => $array['header']['dest_mobile'],
                'dest_phone2'               => $array['header']['dest_phone2'],
                'dest_email'                => $array['header']['dest_email'],
                'ori_country'               => $array['header']['ori_country'],
                'dest_country'              => $array['header']['dest_country'],
                'order_amount'              => $array['header']['order_amount'],
                'shipping_amount'           => $array['header']['shipping_amount'],
                'promo_code'                => $array['header']['promo_code'],
                'insurance_amount'          => $array['header']['insurance_amount']
            ); 
            $order = Order::create($masukHeader);
            return $order->id;
    }

    private function saveOrderDetail($invoiceId, $invoiceDetailId,$headerId, $array){
        $masukDetail = [];
        foreach($array['details'] as $d){
            $masukDetail[] = array(
                'invoice_files_id'          => $invoiceId,
                'order_id'                  => $headerId,
                'order_detail_id'           => $d['order_header_id'],
                'order_header_id'           => $d['order_header_id'],
                'sku_code'                  => $d['sku_code'],
                'sku_description'           => $d['sku_description'],
                'qty_order'                 => $d['qty_order'],
                'price'                     => $d['price'],
                'amount_order'              => $d['amount_order'],
                'qty_ship'                  => $d['qty_ship'],
                'amount_ship'               => $d['amount_ship'],
                'remarks'                   => $d['remarks'],
                'status'                    => $d['status'],
                'create_time'               => $d['create_time'],
                'update_time'               => $d['update_time'],
                'insured'                   => $d['insured'],
                'sku_parent'                => $d['sku_parent'],
                'order_price'               => $d['order_price']
            ); 
        }
        
         OrderDetail::insert($masukDetail); 
}

}
