<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Response,View,Input,Auth,Session,Validator,File,Hash,DB,Mail,Storage;
use Illuminate\Support\Facades\Crypt;


use App\Models\LogActivity;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\Order;
use App\Models\OrderDetail;
use PHPExcel; 
use PHPExcel_IOFactory; 


use App\Jobs\ExtractFiles;

class InvoiceDetailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index(Request $request,$id)
    {
        $perPage = $request->per_page;
        $filterAwb = $request->filterAwb;
        $filterCompany = $request->filterCompany;
        $filterStatus = $request->filterStatus;
        $min = $request->min;
        $max = $request->max;
        $query = InvoiceDetail::with('order')->where('invoice_files_id',$id)->orderBy('id','ASC');
        if ($filterCompany) {
            $likeCompany = "%{$filterCompany}%"; 
            $query = $query->whereHas('order', function($q)use($likeCompany){
                $q->where('company_id', 'LIKE', $likeCompany);
            });
        }
        
        if ($filterAwb) {
            $likeAwb = "%{$filterAwb}%"; 
            $query = $query->where('awb_no', 'LIKE', $likeAwb);
        } 
         
        if ($filterStatus) { 
            $query = $query->where('status', $filterStatus);
        } 

        return $query->paginate($perPage);
    }

    public function downloadData(Request $request)
    { 
        $id = $request->id;
        $filterAwb = $request->filterAwb;
        $filterCompany = $request->filterCompany;
        $filterStatus = $request->filterStatus;
        $filename = $request->filename;
        $query = InvoiceDetail::with('order')->where('invoice_files_id',$id)->orderBy('id','ASC');
        if ($filterCompany) {
            $likeCompany = "%{$filterCompany}%"; 
            $query = $query->whereHas('order', function($q)use($likeCompany){
                $q->where('company_id', 'LIKE', $likeCompany);
            });
        }
        
        if ($filterAwb) {
            $likeAwb = "%{$filterAwb}%"; 
            $query = $query->where('awb_no', 'LIKE', $likeAwb);
        } 
         
        if ($filterStatus) { 
            $query = $query->where('status', $filterStatus);
        } 

        $res =  $query->get();

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()
        ->setCellValue('A1', 'Company ID')
        ->setCellValue('B1', 'Company Name')
        ->setCellValue('C1', 'Courier ID')
        ->setCellValue('D1', 'Order No')
        ->setCellValue('E1', 'AWB No')
        ->setCellValue('F1', 'Order Source')
        ->setCellValue('G1', 'Fulfillment Center ID') 
        ->setCellValue('H1', 'Invoice Customer Code')
        ->setCellValue('I1', 'Invoice Service')
        ->setCellValue('J1', 'Invoice Qty')
        ->setCellValue('K1', 'Invoice Weight Total')
        ->setCellValue('L1', 'Invoice Bag Total')
        ->setCellValue('M1', 'Invoice Weight Price')
        ->setCellValue('N1', 'Invoice COD Amount')
        ->setCellValue('O1', 'Invoice Insurance Amount')
        ->setCellValue('P1', 'Invoice Other Amount')
        ->setCellValue('Q1', 'Invoice Total')
        ->setCellValue('R1', 'Order Amount')
        ->setCellValue('S1', 'Order Shipping Amount')  
        ->setCellValue('T1', 'Order Insurance Amount')
        ->setCellValue('U1', 'Status AWB')
        ->setCellValue('V1', 'Publish Price By Destination')
        ->setCellValue('W1', 'Publish Price By Weight')
        ->setCellValue('X1', '8commerce Price By 3 Percent')
        ->setCellValue('Y1', '8commerce Price By 5 Percent')
        ->setCellValue('AA1', '8commerce Price By 10 Percent')
        ->setCellValue('AB1', '8commerce Price By Destination')
        ->setCellValue('AC1', '8commerce Price By Weight')
        ->setCellValue('AD1', '8commerce Price By Range')
        ->setCellValue('AE1', 'Customer Price By Discount')
        ->setCellValue('AF1', 'Customer Price By Destination')
        ->setCellValue('AG1', 'Customer Price By Weight')
        ;

        $objPHPExcel->getActiveSheet()->getStyle('A1:AG1')->getFont()->setBold(true);  
        $no=1;
        $row=2; 
        foreach ($res as $a){ 
		
		if(isset($a->order->company_id))
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $a->order->company_id);
		
		if(isset($a->order->company_name))
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $a->order->company_name);
		
		if(isset($a->order->courier_id))
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $a->order->courier_id);
		
		if(isset($a->order->order_no))
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $a->order->order_no);
		
		if(isset($a->order->awb_no))
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $a->order->awb_no);
		
		if(isset($a->order->order_source))
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $a->order->order_source);
		
		if(isset($a->order->fulfillment_center_id))
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $a->order->fulfillment_center_id);
		
		if(isset($a->customer_code))
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $a->customer_code);
        
		if(isset($a->service))
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$row, $a->service);
        
		if(isset($a->qty))
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$row, $this->IsNullOrEmptyString($a->qty));
        
		if(isset($a->weight_total))
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$row, $this->IsNullOrEmptyString($a->weight_total));
        
		if(isset($a->bag_total))
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$row, $this->IsNullOrEmptyString($a->bag_total));
        
		if(isset($a->weight_price))
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$row, $this->IsNullOrEmptyString($a->weight_price));
        
		if(isset($a->cod_amount))
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$row, $this->IsNullOrEmptyString($a->cod_amount));
        
		if(isset($a->insurance_amount))
			$objPHPExcel->getActiveSheet()->setCellValue('O'.$row, $this->IsNullOrEmptyString($a->insurance_amount));
        
		if(isset($a->other_amount))
			$objPHPExcel->getActiveSheet()->setCellValue('P'.$row, $this->IsNullOrEmptyString($a->other_amount));
        
		if(isset($a->price))
			$objPHPExcel->getActiveSheet()->setCellValue('Q'.$row, $this->IsNullOrEmptyString($a->price));
			
		if(isset($a->order->order_amount))
		   $objPHPExcel->getActiveSheet()->setCellValue('R'.$row, $this->IsNullOrEmptyString($a->order->order_amount));
            
		if(isset($a->order->shipping_amount))
		   $objPHPExcel->getActiveSheet()->setCellValue('S'.$row, $this->IsNullOrEmptyString($a->order->shipping_amount));
            
	    if(isset($a->order->insurance_amount))
			$objPHPExcel->getActiveSheet()->setCellValue('T'.$row, $this->IsNullOrEmptyString($a->order->insurance_amount));

        if(isset($a->publishPriceByDestination))
            $objPHPExcel->getActiveSheet()->setCellValue('V'.$row, $this->IsNullOrEmptyString($a->publishPriceByDestination));

        if(isset($a->publishPriceByWeight))
            $objPHPExcel->getActiveSheet()->setCellValue('W'.$row, $this->IsNullOrEmptyString($a->publishPriceByWeight));

        if(isset($a->commercePriceBy3Percent))
            $objPHPExcel->getActiveSheet()->setCellValue('X'.$row, $this->IsNullOrEmptyString($a->commercePriceBy3Percent));

        if(isset($a->commercePriceBy5Percent))
            $objPHPExcel->getActiveSheet()->setCellValue('Y'.$row, $this->IsNullOrEmptyString($a->commercePriceBy5Percent));

        if(isset($a->commercePriceBy10Percent))
            $objPHPExcel->getActiveSheet()->setCellValue('Z'.$row, $this->IsNullOrEmptyString($a->commercePriceBy10Percent));

        if(isset($a->commercePriceByDestination))
            $objPHPExcel->getActiveSheet()->setCellValue('AA'.$row, $this->IsNullOrEmptyString($a->commercePriceByDestination));

        if(isset($a->commercePriceByWeight))
            $objPHPExcel->getActiveSheet()->setCellValue('AB'.$row, $this->IsNullOrEmptyString($a->commercePriceByWeight));

        if(isset($a->commercePriceByRange))
            $objPHPExcel->getActiveSheet()->setCellValue('AC'.$row, $this->IsNullOrEmptyString($a->commercePriceByRange));

        if(isset($a->customerPriceByDiscount))
            $objPHPExcel->getActiveSheet()->setCellValue('AD'.$row, $this->IsNullOrEmptyString($a->customerPriceByDiscount));

        if(isset($a->customerPriceByDestination))
            $objPHPExcel->getActiveSheet()->setCellValue('AE'.$row, $this->IsNullOrEmptyString($a->customerPriceByDestination));

        if(isset($a->customerPriceByWeight))
            $objPHPExcel->getActiveSheet()->setCellValue('AF'.$row, $this->IsNullOrEmptyString($a->customerPriceByWeight));

            
		$objPHPExcel->getActiveSheet()->setCellValue('U'.$row, $a->status);
        $no++; 
        $row++;   
        }
        
        $objPHPExcel->getActiveSheet()->setTitle('Sheet1');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"'); 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    
    public function downloadDataByCompany(Request $request)
    { 
        $id = $request->id;
        $filterAwb = $request->filterAwb;
        $filterCompany = $request->filterCompany;
        $filterStatus = $request->filterStatus;
        $filename = $request->filename;
        $query = InvoiceDetail::with('order')->where('invoice_files_id',$id)->orderBy('id','ASC');
        if ($filterCompany) { 
            $query = $query->whereHas('order', function($q)use($likeCompany){
                $q->where('company_id', $likeCompany);
            });
        }
        
        if ($filterAwb) {
            $likeAwb = "%{$filterAwb}%"; 
            $query = $query->where('awb_no', 'LIKE', $likeAwb);
        } 
         
        if ($filterStatus) { 
            $query = $query->where('status', $filterStatus);
        } 

        $res =  $query->get();

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()
        ->setCellValue('A1', 'Company ID')
        ->setCellValue('B1', 'Company Name')
        ->setCellValue('C1', 'Courier ID')
        ->setCellValue('D1', 'Order No')
        ->setCellValue('E1', 'AWB No')
        ->setCellValue('F1', 'Order Source')
        ->setCellValue('G1', 'Fulfillment Center ID') 
        ->setCellValue('H1', 'Invoice Customer Code')
        ->setCellValue('I1', 'Invoice Service')
        ->setCellValue('J1', 'Invoice Qty')
        ->setCellValue('K1', 'Invoice Weight Total')
        ->setCellValue('L1', 'Invoice Bag Total')
        ->setCellValue('M1', 'Invoice Weight Price')
        ->setCellValue('N1', 'Invoice COD Amount')
        ->setCellValue('O1', 'Invoice Insurance Amount')
        ->setCellValue('P1', 'Invoice Other Amount')
        ->setCellValue('Q1', 'Invoice Total')
        ->setCellValue('R1', 'Order Amount')
        ->setCellValue('S1', 'Order Shipping Amount')  
        ->setCellValue('T1', 'Order Insurance Amount')
        ->setCellValue('U1', 'Status AWB')
        ;

        $objPHPExcel->getActiveSheet()->getStyle('A1:U1')->getFont()->setBold(true);  
        $no=1;
        $row=2; 
        foreach ($res as $a){
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $a->order->company_id);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $a->order->company_name);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $a->order->courier_id);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $a->order->order_no);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $a->order->awb_no);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $a->order->order_source);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $a->order->fulfillment_center_id);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $a->customer_code);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$row, $a->service);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$row, $this->IsNullOrEmptyString($a->qty));
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$row, $this->IsNullOrEmptyString($a->weight_total));
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$row, $this->IsNullOrEmptyString($a->bag_total));
            $objPHPExcel->getActiveSheet()->setCellValue('M'.$row, $this->IsNullOrEmptyString($a->weight_price));
            $objPHPExcel->getActiveSheet()->setCellValue('N'.$row, $this->IsNullOrEmptyString($a->cod_amount));
            $objPHPExcel->getActiveSheet()->setCellValue('O'.$row, $this->IsNullOrEmptyString($a->insurance_amount));
            $objPHPExcel->getActiveSheet()->setCellValue('P'.$row, $this->IsNullOrEmptyString($a->other_amount));
            $objPHPExcel->getActiveSheet()->setCellValue('Q'.$row, $this->IsNullOrEmptyString($a->price));
            $objPHPExcel->getActiveSheet()->setCellValue('R'.$row, $this->IsNullOrEmptyString($a->order->order_amount));
            $objPHPExcel->getActiveSheet()->setCellValue('S'.$row, $this->IsNullOrEmptyString($a->order->shipping_amount));
            $objPHPExcel->getActiveSheet()->setCellValue('T'.$row, $this->IsNullOrEmptyString($a->order->insurance_amount));
            $objPHPExcel->getActiveSheet()->setCellValue('U'.$row, $a->status);
        $no++; 
        $row++;   
        }
        
        $objPHPExcel->getActiveSheet()->setTitle('Sheet1');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"'); 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    private function IsNullOrEmptyString($str){
        if(!isset($str) || trim($str) === ''){
            return 0;
        }else{
            return $str;
        }
    }

}