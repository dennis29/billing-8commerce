<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Response,View,Input,Session,Validator,File,Hash,DB,Mail;
use Illuminate\Support\Facades\Crypt;


use App\Models\LogActivity;
use App\Models\Invoice;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


use App\Jobs\ExtractFiles;
//JobController
trait JobsFile
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         
    }

    public static function extractFiles($data){ 
        $cek = Invoice::findOrFail($data['id']);
		echo $cek;
        $ExtractFiles = (new ExtractFiles($cek))->delay(Carbon::now()->addMinutes(1));
        dispatch($ExtractFiles); 
        $cek->update(array('status' => 'processing'));      
    } 
}