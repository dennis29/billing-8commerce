<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Response,View,Input,Auth,Session,Validator,File,Hash,DB,Mail,Storage;
use Illuminate\Support\Facades\Crypt;


use App\Models\LogActivity;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\OrderDetail;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


use App\Jobs\ExtractFiles;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function detail($id)
    { 
        $cek = Order::with('details')->where('invoice_file_details_id',$id)->first();
        if(!$cek)
        {
            return response()->json(['status'=>404,'data'=>'','message'=>['error'=>['Data Not Found']]]);
        }else{ ;
            return response()->json(['status'=>200,'data'=>$cek,'message'=>'']);
        }
    }
    
    public function getByCompany(Request $request,$id,$company)
    { 
        $perPage = $request->per_page;
        $filterAwb = $request->filterAwb; 
        $min = $request->min;
        $max = $request->max;
        $query = Order::with('invoice','details')->where([['invoice_files_id',$id],['company_id',$company]])->orderBy('id','ASC');
            
        if ($filterAwb) {
            $likeAwb = "%{$filterAwb}%"; 
            $query = $query->where('awb_no', 'LIKE', $likeAwb)->orWhere('order_no', 'LIKE', $likeAwb);
        }  

        return $query->paginate($perPage);
    }

}