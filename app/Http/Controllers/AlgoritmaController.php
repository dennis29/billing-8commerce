<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use Response,View,Input,Auth,Session,Validator,File,Hash,DB,Excel,Mail;
use Illuminate\Support\Facades\Crypt;

use App\Models\LogActivity;
use App\Models\algoritma;

class AlgoritmaController extends Controller
{
    public function getAlgoritma()
    { 
        $data = algoritma::orderBy('name','ASC')->get(); 
        return response()->json(['status'=>200,'data'=>$data,'message'=>'']); 
    }
}
