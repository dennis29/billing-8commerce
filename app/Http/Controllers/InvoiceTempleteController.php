<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Response,View,Input,Auth,Session,Validator,File,Hash,DB,Mail,Storage;
use Illuminate\Support\Facades\Crypt;


use App\Models\LogActivity;
use App\Models\InvoiceTemplete; 
use App\Models\Courier;

class InvoiceTempleteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $perPage = $request->per_page;
        $search = $request->filter; 
        $query = InvoiceTemplete::with('courier')->orderBy('id','DESC');
        if ($search) {
            $like = "%{$search}%";
            $query = $query->where('templete_name', 'LIKE', $like);
        } 
         
        return $query->paginate($perPage);
    }

    public function store(Request $request)
    {   
        $valid = $this->validate($request, [ 
            'courier_id'    => 'required|numeric|not_in:0|unique:invoice_templete,courier_id', 
            'templete_name'     => 'required|mimes:xlsx,xls|file|max:10000000'
        ]); 

        $courier =  Courier::where('id',$request->courier_id)->first();
        $extension  = $request->templete_name->extension();  
        $fileName   = $courier->courier_code.'.'.$extension; // renameing image
        $path = public_path('/courier-invoice-templete/'); // upload path
        
        if(file_exists($path.$fileName)){
            File::delete($path.$fileName);
        } 
        
        $upload_success     = $request->templete_name->move($path,$fileName); 
        
        if(!$upload_success){
            return response()->json(['status'=>422,'data'=>'','message'=>['templete_name'=>['Upload failed']]]); 
        }else{
            $masuk = array('courier_id' => $request->courier_id,  'templete_name' => $fileName); 
            InvoiceTemplete::create($masuk);
            LogActivity::create(['name' => Auth::user()->id, 'email' => Auth::user()->email, 'table'=>'invoice_templete' ,'action' => 'insert', 'data' => json_encode($masuk)]);
            return response()->json(['status'=>200,'data'=>'','message'=>'Add Successfully']);
        }   
    }


    public function destroy($id)
    {
        $cek = InvoiceTemplete::findOrFail($id);
        if(!$cek)
        {
            return response()->json(['status'=>404,'data'=>'','message'=>['error'=>['Data Not Found']]]);
        }else{             
            $path = public_path('/courier-invoice-templete/'); // upload path
            LogActivity::create(['name' => Auth::user()->id, 'email' => Auth::user()->email, 'table'=>'invoice_templete' ,'action' => 'delete', 'data' => json_encode($cek)]);
            InvoiceTemplete::where('id',$id)->delete();              
            File::delete($path.$cek->templete_name);       
            return response()->json(['status'=>200,'data'=>'','message'=>'Delete Successfully']);
  
        } 

    }

    
    public function downloadFile(Request $request){
        $fileName = $request->filename;
        $path = public_path().'/courier-invoice-templete/'.$fileName; 
        return response()->download($path); 
    }

    public function update(Request $request)
    { 
        
    }

     

}