<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Response,View,Input,Auth,Session,Validator,File,Hash,DB,Mail,Storage;
use Illuminate\Support\Facades\Crypt;


use App\Models\LogActivity;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\Order;
use App\Models\OrderDetail;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
  
use App\Http\Controllers\JobsFile;

class InvoiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $perPage = $request->per_page;
        $search = $request->filter;
		$courier = $request->courier;
        $min = $request->min;
        $max = $request->max;
        $query = Invoice::with('courier')->orderBy('id','DESC');
		//dd($query->toSql(), $query->getBindings());
        if ($search) {
            $like = "%{$search}%";
            $query = $query->where('name', 'LIKE', $like);
        }
		if ($courier) {
            $likex = "%{$courier}%";
            $query = $query->where('invoice_files.courier_id', 'LIKE', $likex);
        }
        if($min && !$max)
        {
            $query = $query->whereDate('created_at','=',$min);
        }
        if(!$min && $max)
        {
            $query = $query->whereDate('created_at','=',$max);
        }
        if($min && $max)
        {
            $query = $query->whereDate('created_at','>=',$min)->whereDate('created_at','<=',$max);
        }
         
        return $query->paginate($perPage);
    }

    public function store(Request $request)
    {  
        ini_set('memory_limit','-1');

        $valid = $this->validate($request, [ 
            'courier_id'    => 'required|numeric|not_in:0',
	        'name'          => 'required|max:255', 
            'date_invoice'  => 'required|date_format:m/Y',
            'status'        => 'in:upload,process,complete', 
            'file_name'     => 'required|mimes:xlsx,xls|file|max:10000000'
        ]);

        $check = Invoice::where([['courier_id',$request->courier_id],['date_invoice',$request->date_invoice]])->first();
        if(!$check){ 
            $extension  = $request->file_name->extension();  
            $fileName   = str_replace(' ', '-', $request->name).'-'.time().'.'.$extension; // renameing image
            $path = public_path('/invoice/'); // upload path
                if(file_exists($path.$fileName)){
                    File::delete($path.$fileName);
                } 
                    $upload_success     = $request->file_name->move($path,$fileName); 
                    if(!$upload_success){
                        return response()->json(['status'=>422,'data'=>'','message'=>['file_name'=>['Upload failed']]]); 
                    }else{
                        $masuk = array('courier_id' => $request->courier_id, 'name' => $request->name, 'date_invoice' => $request->date_invoice, 'status' => $request->status, 'file_name' => $fileName, 'remarks'=>'', 'data'=>''); 
                        //dd($masuk);
						$insert = Invoice::create($masuk);
						//dd($insert->id);
                        LogActivity::create(['name' => Auth::user()->id, 'email' => Auth::user()->email, 'table'=>'invoice_files' ,'action' => 'insert', 'data' => json_encode($masuk)]);
                        JobsFile::extractFiles($insert);
						
                        return response()->json(['status'=>200,'data'=>'','message'=>'Add Successfully']);
                    } 
        }else{
            return response()->json(['status'=>422,'data'=>'','message'=>['error'=>['Data Match Found For courier and date']]]);
        } 
    }
		
    public function destroy($id)
    {
        $cek = Invoice::findOrFail($id);
        if(!$cek)
        {
            return response()->json(['status'=>404,'data'=>'','message'=>['error'=>['Data Not Found']]]);
        }else{             
            $path = public_path('/invoice/'); // upload path
            LogActivity::create(['name' => Auth::user()->id, 'email' => Auth::user()->email, 'table'=>'invoice' ,'action' => 'delete', 'data' => json_encode($cek)]);
            Invoice::where('id',$id)->delete();              
            File::delete($path.$cek->file_name);      
            InvoiceDetail::where('invoice_files_id',$id)->delete();
            Order::where('invoice_files_id',$id)->delete();
            OrderDetail::where('invoice_files_id',$id)->delete();
            return response()->json(['status'=>200,'data'=>'','message'=>'Delete Successfully']);
  
        } 

    }

    public function update(Request $request)
    {
        $cek = Invoice::findOrFail($request->id);
        if(!$cek)
        {
            return response()->json(['status'=>404,'data'=>'','message'=>['error'=>['Data Not Found']]]);
        }else{
            ini_set('memory_limit','-1');
            if($request->file_name == ''){
                $valid = $this->validate($request, [ 
                    'courier_id'    => 'required|numeric|not_in:0',
			        'name'          => 'required|max:255', 
                    'date_invoice'  => 'required|date_format:m/Y',
                    'status'        => 'in:upload,process,complete'
                ]);
                if($request->courier_id == $cek->courier_id){
                    $edit = array('courier_id' => $request->courier_id, 'name' => $request->name, 'date_invoice' => $request->date_invoice); 
                    $cek->update($edit);
                    LogActivity::create(['name' => Auth::user()->id, 'email' => Auth::user()->email, 'table'=>'invoice' ,'action' => 'update', 'data' => json_encode($cek)]);
                    return response()->json(['status'=>200,'data'=>'','message'=>'Edit Successfully']);

                }else{
                    $edit = array('courier_id' => $request->courier_id, 'name' => $request->name, 'date_invoice' => $request->date_invoice, 'status' => 'upload');
                    $cek->update($edit);
                    InvoiceDetail::where('invoice_files_id',$request->id)->delete();
                    Order::where('invoice_files_id',$request->id)->delete();
                    OrderDetail::where('invoice_files_id',$request->id)->delete();
                    LogActivity::create(['name' => Auth::user()->id, 'email' => Auth::user()->email, 'table'=>'invoice' ,'action' => 'update', 'data' => json_encode($cek)]);
                    return response()->json(['status'=>200,'data'=>'','message'=>'Edit Successfully']);
                } 
            }else{ 
                $valid = $this->validate($request, [ 
                    'courier_id'    => 'required|numeric|not_in:0',
                    'name'          => 'required|max:255', 
                    'date_invoice'  => 'required|date_format:m/Y',
                    'status'        => 'in:upload,process,complete', 
                    'file_name'     => 'required|mimes:xlsx,xls|file|max:10000000'
                ]);

                $extension  = $request->file_name->extension();  
                $fileName   = str_replace(' ', '-', $request->name).'-'.time().'.'.$extension; // renameing image
                $path = public_path('/invoice/'); // upload path
                if(file_exists($path.$fileName)){
                    File::delete($path.$fileName);
                } 
                
                $upload_success     = $request->file_name->move($path,$fileName); 
                if(!$upload_success){
                    return response()->json(['status'=>422,'data'=>'','message'=>['file_name'=>['Upload failed']]]); 
                }else{
                    File::delete($path.$cek->file_name);
                    $edit = array('courier_id' => $request->courier_id, 'name' => $request->name, 'date_invoice' => $request->date_invoice, 'status' => $request->status, 'file_name' => $fileName, 'remarks'=>'', 'data'=>''); 
                    $cek->update($edit);
                    InvoiceDetail::where('invoice_files_id',$request->id)->delete();
                    Order::where('invoice_files_id',$request->id)->delete();
                    OrderDetail::where('invoice_files_id',$request->id)->delete();
                    LogActivity::create(['name' => Auth::user()->id, 'email' => Auth::user()->email, 'table'=>'invoice' ,'action' => 'update', 'data' => json_encode($cek)]);
                    JobsFile::extractFiles($request->post());
					return response()->json(['status'=>200,'data'=>'','message'=>'Edit Successfully']);
                } 
            } 
        } 
    }

    
    public function getCourier()
    { 
        $data = Courier::orderBy('id','ASC')->get(); 
            return response()->json(['status'=>200,'data'=>$data,'message'=>'']); 
    } 
 
    public function downloadFile(Request $request){
        $fileName = $request->filename;
        $path = public_path().'/invoice/'.$fileName; 
        return response()->download($path); 
    }

    public function getById($id)
    {
        $cek = Invoice::with('courier')->findOrFail($id);
        if(!$cek)
        {
            return response()->json(['status'=>404,'data'=>'','message'=>['error'=>['Data Not Found']]]);
        }else{
            return response()->json(['status'=>200,'data'=>$cek,'message'=>'']);
        }
    }

}