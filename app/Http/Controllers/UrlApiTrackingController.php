<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UrlApiTrackingController extends UltimateController
{
	public $arr = array();
	public $replace = array();
    
    function __construct()
    {
		$this->db_tbl = new \App\Models\Courier();
		$this->replace = array("%2F","%20","&");
		$this->arrDetail = [];
		$this->arr[0]['courier_code'] = "-";
		$this->arr[0]['awb_no'] = "-";
		$this->arr[0]['service'] = "-";
		$this->arr[0]['destination'] = "-";
		$this->arr[0]['sender'] = "8Commerce";
		$this->arr[0]['date_received'] = "-";
		$this->arr[0]['receiver'] = "-";
		$this->arr[0]['status'] = "-";
		$this->succeed = true;
    }

    public function getBody(Request $request){
		$arr = $request->post();
		//print_r($arr);die();
		if(isset($arr['courier_code']) && isset($arr['awb_no'])){
			$arr['courier_code'] = str_replace($this->replace,"", $arr['courier_code']);
			$function = "tracking_".$arr['courier_code'];
			if(method_exists($this, $function)){
			  //  echo $this->url_method;die();
				$this->arr[0]['courier_code'] = strtoupper($arr['courier_code']);
				$this->arr[0]['awb_no'] = strtoupper($arr['awb_no']);
				$this->$function($arr);
				return $this->db_tbl->Response::json(['status'=>200,'data'=>$this->arr,'message'=>'']);
			}
		}
	}

	public function getDetailForTrackingAWB(Request $request){
		$arr = $request->post();
		//print_r($arr);die();
		if(isset($arr['courier_code']) && isset($arr['awb_no'])){
			$arr['courier_code'] = str_replace($this->replace,"", $arr['courier_code']);
			$function = "trackingDetail_".$arr['courier_code'];
			if(method_exists($this, $function)){
			  //  echo $this->url_method;die();
				$this->arr[0]['awb_no'] = strtoupper($arr['awb_no']);
				$this->$function();
				return $this->db_tbl->Response::json(['status'=>200,'data'=>$this->arrDetail,'message'=>'']);
			}
		}
	}

	public function tracking_wahana($array=[]){
		/*

		$res = $this->db_tbl->Client->request('GET', "http://p.wahana.com:8168/cgi-bin/ws.cgi?svc=telusur&ptnrId=kawankami&ptnrKey=welcome&ttkNo=".$array['awb_no']);
	//		echo $res->getBody();die();
		*/

		$arry = json_decode(json_encode(simplexml_load_string($this->myUrl("http://p.wahana.com:8168/cgi-bin/ws.cgi?svc=telusur&ptnrId=kawankami&ptnrKey=welcome&ttkNo=".$array['awb_no'])->getAPI()->getBody())),TRUE);

		if(!isset($arry['xmlMessage']['root']['data'])){
            die();
		}

		$arr = end($arry['xmlMessage']['root']['data']);

		$this->service("SVC");
		if(isset($arr['ttkPenerimaAlamatLengkap'])){
			$this->destination($arr['ttkPenerimaAlamatLengkap']);
		}
		if(isset($arr['ttkPengirimNama'])){
			$this->sender($arr['ttkPengirimNama']);
		}
		if(isset($arr['ttkTrackRecDateTime'])){
			$this->date_received(date('d M Y H:i:s',strtotime($arr['ttkTrackRecDateTime'])));
		}
		if(isset($arr['ttkTrackDiterimaOleh'])){
			$this->receiver($arr['ttkTrackDiterimaOleh']);
		}

		if(isset($arry['xmlMessage']['ttklstnm'])){
			$this->status($arry['xmlMessage']['ttklstnm']);	
		}

	}

	public function trackingDetail_wahana(){

		//echo json_encode(simplexml_load_string($this->myUrl("http://p.wahana.com:8168/cgi-bin/ws.cgi?svc=telusur&ptnrId=kawankami&ptnrKey=welcome&ttkNo=".$this->arr[0]['awb_no'])->getAPI()->getBody()));die();

		$arry = json_decode(json_encode(simplexml_load_string($this->myUrl("http://p.wahana.com:8168/cgi-bin/ws.cgi?svc=telusur&ptnrId=kawankami&ptnrKey=welcome&ttkNo=".$this->arr[0]['awb_no'])->getAPI()->getBody())),TRUE);

		if(!isset($arry['xmlMessage']['root']['data'])){
            die();
		}

		foreach($arry['xmlMessage']['root']['data'] as $k => $v){

	    	$this->defArrayDetailHistory($k);

	    	if(isset($v['ttkTrackRecDateTime']))
	    	{
	    		$this->checkDataHistory($k,date('d M Y H:i:s',strtotime($v['ttkTrackRecDateTime'])),'datetime');
	    	}

	    	if(isset($v['ttkTrackStatusKode']))
	    	{
	    		$this->checkDataHistory($k,$v['ttkTrackStatusKode'],'status');
	    	}

	    	if(isset($v['ttkTrackStatusNama']))
	    	{
	    		$this->checkDataHistory($k,$v['ttkTrackStatusNama'],'note');
	    	}	
	    }	
	    

	}

	public function tracking_jt($array=[]){
		/*
		$res = $this->db_tbl->Client->request('GET', 'http://interchange.jet.co.id:22268/jandt-order-web/track/trackAction!tracking.action', [
		    'auth' => ['8COMM', '8c0mM8o72qksjdfhsvb'],'json' => ['awb'=>$array['awb_no'],'eccompanyid'=>'8COMM'],
		]);
		*/
		$body = json_decode($this->myUrl("http://interchange.jet.co.id:22268/jandt-order-web/track/trackAction!tracking.action")->myUrlParams([
		    'auth' => ['8COMM', '8c0mM8o72qksjdfhsvb'],'json' => ['awb'=>$array['awb_no'],'eccompanyid'=>'8COMM']])->getAPI()->getBody(), TRUE);

		//print_r($body);die();

	    if(isset($body['error_id'])){
	    	die();
	    }

		$end_arr  = end($body['history']);

		$this->service("EZ");
		if(isset($end_arr['receiver']))
		{
			$this->receiver($end_arr['receiver']);
		}	

		if(isset($body['detail']['sender']['name']))
		{
			$this->sender($body['detail']['sender']['name']);
		}

		if(isset($body['detail']['receiver']['addr']))
		{
			$this->destination($body['detail']['receiver']['addr']);
		}

		if(isset($end_arr['date_time']))
		{
			$this->date_received(date('d M Y H:i:s',strtotime($end_arr['date_time'])));
		}

		if(isset($end_arr['status']))
		{
			$this->status($end_arr['status']);
		}

   	}

   	public function trackingDetail_jt(){

   //echo $this->myUrl("http://interchange.jet.co.id:22268/jandt-order-web/track/trackAction!tracking.action")->myUrlParams(['auth' => ['8COMM', '8c0mM8o72qksjdfhsvb'],'json' => ['awb'=>$this->arr[0]['awb_no'],'eccompanyid'=>'8COMM']])->getAPI()->getBody();die();

		$body = json_decode($this->myUrl("http://interchange.jet.co.id:22268/jandt-order-web/track/trackAction!tracking.action")->myUrlParams([
		    'auth' => ['8COMM', '8c0mM8o72qksjdfhsvb'],'json' => ['awb'=>$this->arr[0]['awb_no'],'eccompanyid'=>'8COMM']])->getAPI()->getBody(), TRUE);

	    if(isset($body['error_id'])){
	    	die();
	    }

	    //print_r($body['history']);die();


		foreach($body['history'] as $k => $v){

	    	$this->defArrayDetailHistory($k);

	    	if(isset($v['date_time']))
	    	{
	    		$this->checkDataHistory($k,date('d M Y H:i:s',strtotime($v['date_time'])),'datetime');
	    	}

	    	if(isset($v['status']))
	    	{
	    		$this->checkDataHistory($k,$v['status'],'status');
	    	}

	    	if(isset($v['note']))
	    	{
	    		$this->checkDataHistory($k,$v['note'],'note');
	    	}	
	    	
	    }

   	}

   	public function tracking_sap($array=[]){
   		/*

		$res = $this->db_tbl->Client->request('GET', 'http://track.coresyssap.com/shipment/tracking/awb?awb_no='.$array['awb_no'], [
    'headers' => [
        'api_key'	 => 'B8Ecomm3rCe_#_2019'
    ]
]);
        */
		$body = json_decode($this->myUrl('http://track.coresyssap.com/shipment/tracking/awb?awb_no='.$array['awb_no'])->myUrlParams(['headers' => ['api_key' => 'B8Ecomm3rCe_#_2019']])->getAPI()->getBody(), TRUE);

		if($body == []){
	    	die();
	    }

		$end_arr = end($body);

		$this->service("EZ");
		if(isset($end_arr['receiver_name']))
		{
			$this->receiver($end_arr['receiver_name']);
		}	

		if(isset($end_arr['destination']))
		{
			$this->destination($end_arr['destination']);
		}

		if(isset($end_arr['create_date']))
		{
			$this->date_received(date('d M Y H:i:s',strtotime($end_arr['create_date'])));
		}

		if(isset($end_arr['description']))
		{
			$arr_status = explode('[',str_replace(']', '', $end_arr['description']));
			$status = explode('-',str_replace('STATUS:', '', $arr_status[4]))[0];
			$this->status($status);
		}

   	}

   	public function trackingDetail_sap(){
   		/*

		$res = $this->db_tbl->Client->request('GET', 'http://track.coresyssap.com/shipment/tracking/awb?awb_no='.$array['awb_no'], [
    'headers' => [
        'api_key'	 => 'B8Ecomm3rCe_#_2019'
    ]
]);
        */

		$body = json_decode($this->myUrl('http://track.coresyssap.com/shipment/tracking/awb?awb_no='.$this->arr[0]['awb_no'])->myUrlParams(['headers' => ['api_key' => 'B8Ecomm3rCe_#_2019']])->getAPI()->getBody(), TRUE);

		if($body == []){
	    	die();
	    }

		$end_arr = end($body);

		foreach($body as $k => $v){

	    	$this->defArrayDetailHistory($k);

	    	if(isset($v['create_date']))
	    	{
	    		$this->checkDataHistory($k,date('d M Y H:i:s',strtotime($v['create_date'])),'datetime');
	    	}

	    	if(isset($v['rowstate_name']))
	    	{
	    		$this->checkDataHistory($k,$v['rowstate_name'],'status');
	    	}

	    	if(isset($v['description']))
	    	{
	    		$this->checkDataHistory($k,$v['description'],'note');
	    	}	
	    	
	    }

   	}

   	public function tracking_tiki($array=[]){
   		/*
		$res = $this->db_tbl->Client->request('POST', 'http://apis.mytiki.net:8321/user/auth', ['json' => ['username'=>'8COMMERCE','password'=>'8COMMERC321'],
		]);
		$res = $this->db_tbl->Client->request('POST', 'http://apis.mytiki.net:8321/connote/info', [
    'headers' => [
        'x-access-token'	 => $body['response']['token']
    ],
    'json' => [
        'cnno'	 => $array['awb_no']
    ],
]);
        */
		$body = json_decode($this->myUrlParams(['headers' => ['x-access-token' => json_decode($this->myUrlParams(['json' => ['username'=>'8COMMERCE','password'=>'8COMMERC321']])->myUrl('http://apis.mytiki.net:8321/user/auth')->myUrlMethod("POST")->getAPI()->getBody(), TRUE)['response']['token']],'json' => ['cnno' => $array['awb_no']]])->myUrl('http://apis.mytiki.net:8321/connote/info')->myUrlMethod("POST")->getAPI()->getBody(), TRUE);	

		if(!$body['response']){
	    	die();
	    }

	    $end_res = $body['response'][0];

		if(isset($end_res['product']))
		{
			$this->service($end_res['product']);
		}	

		if(isset($end_res['consignor_name']))
		{
			$this->sender($end_res['consignor_name']);
		}

		if(isset($end_res['consignee_name']))
		{
			$this->receiver($end_res['consignee_name']);
		}	

		if(isset($end_res['consignee_address']))
		{
			$this->destination($end_res['consignee_address']);
		}

		if(isset($end_res['est_date']))
		{
			$this->date_received(date('d M Y H:i:s',strtotime($end_res['est_date'])));
		}

   	}

   	public function tracking_sicepat($array=[]){
   	/*
   	$res = $this->db_tbl->Client->request('GET', 'http://api.sicepat.com/customer/waybill?waybill='.$array['awb_no'], [
    'headers' => [
        'api-key'	 => 'e8f4e9a7b7139b6671776f283865e400'
    ]
]);	
   	$body = json_decode($res->getBody(), TRUE)['sicepat'];
   	*/

		$body = json_decode($this->myUrl('http://api.sicepat.com/customer/waybill?waybill='.$array['awb_no'])->myUrlParams(['headers' => ['api-key'	 => 'e8f4e9a7b7139b6671776f283865e400']])->getAPI()->getBody(), TRUE)['sicepat'];

		if($body['status']['code'] == 400){
	    	die();
	    }

	    $end_arr = $body['result'];

		if(isset($end_arr['service']))
		{
			$this->service($end_arr['service']);
		}	

		if(isset($end_arr['sender']))
		{
			$this->sender($end_arr['sender']);
		}

		if(isset($end_arr['receiver_name']))
		{
			$this->receiver($end_arr['receiver_name']);
		}	

		if(isset($end_arr['receiver_address']))
		{
			$this->destination($end_arr['receiver_address']);
		}

		if(isset($end_arr['last_status']['date_time']))
		{
			$this->date_received(date('d M Y H:i:s',strtotime($end_arr['last_status']['date_time'])));
		}

		if(isset($end_arr['last_status']['status']))
		{
			$this->status($end_arr['last_status']['status']);
		}

   	}

   	public function trackingDetail_sicepat(){

		$body = json_decode($this->myUrl('http://api.sicepat.com/customer/waybill?waybill='.$this->arr[0]['awb_no'])->myUrlParams(['headers' => ['api-key'	 => 'e8f4e9a7b7139b6671776f283865e400']])->getAPI()->getBody(), TRUE)['sicepat'];

		if($body['status']['code'] == 400){
	    	die();
	    }

	    foreach($body['result']['track_history'] as $k => $v){

	    	$this->defArrayDetailHistory($k);

	    	if(isset($v['date_time']))
	    	{
	    		$this->checkDataHistory($k,date('d M Y H:i:s',strtotime($v['date_time'])),'datetime');
	    	}

	    	if(isset($v['status']))
	    	{
	    		$this->checkDataHistory($k,$v['status'],'status');
	    	}

	    	if(isset($v['city']))
	    	{
	    		$this->checkDataHistory($k,$v['city'],'note');
	    	}	
	    	
	    }
		

   	}

   	public function tracking_jne($array=[]){

   		
/*
	   	$res = $this->db_tbl->Client->request('POST', 'http://apiv2.jne.co.id:10101/tracing/api/list/v1/cnote/'.$array['awb_no'], [
	    'form_params' => [
	        'username'	 => 'CIPTAMAPAN',
	        'api_key'	 => '3d741a1026125901c18f9a76d5df743d'
	    ]
	]);
	*/
	   	$body = json_decode($this->myUrl('http://apiv2.jne.co.id:10101/tracing/api/list/v1/cnote/'.$array['awb_no'])->myUrlParams(['form_params' => [
	        'username'	 => 'CIPTAMAPAN',
	        'api_key'	 => '3d741a1026125901c18f9a76d5df743d'
	    ]])->myUrlMethod('POST')->getAPI()->getBody(), TRUE);
	   	
	   	if(isset($body['error'])){
		   die();
		}

		if(isset($body['cnote']['servicetype']))
		{
				$this->service($body['cnote']['servicetype']);
		}	

		if(isset($body['cnote']['cnote_receiver_name']))
		{
				$this->receiver($body['cnote']['cnote_receiver_name']);
		}	

		if(isset($body['cnote']['city_name']))
		{
				$this->destination($body['cnote']['city_name']);
		}

		if(isset($body['cnote']['cnote_pod_date']))
		{
				$this->date_received(date('d M Y H:i:s',strtotime($body['cnote']['cnote_pod_date'])));
		}

		if(isset($body['cnote']['pod_status']))
		{
				$this->status($body['cnote']['pod_status']);
		}

   	}

   	public function trackingDetail_jne(){

   		//echo $this->myUrl('http://apiv2.jne.co.id:10101/tracing/api/list/v1/cnote/'.$this->arr[0]['awb_no'])->myUrlParams(['form_params' => ['username'	 => 'CIPTAMAPAN','api_key'	 => '3d741a1026125901c18f9a76d5df743d']])->myUrlMethod('POST')->getAPI()->getBody();die();


	   	$body = json_decode($this->myUrl('http://apiv2.jne.co.id:10101/tracing/api/list/v1/cnote/'.$this->arr[0]['awb_no'])->myUrlParams(['form_params' => [
	        'username'	 => 'CIPTAMAPAN',
	        'api_key'	 => '3d741a1026125901c18f9a76d5df743d'
	    ]])->myUrlMethod('POST')->getAPI()->getBody(), TRUE);
	   	
	   	if(isset($body['error'])){
		   die();
		}

		foreach($body['history'] as $k => $v){

	    	$this->defArrayDetailHistory($k);

	    	if(isset($v['date']))
	    	{
	    		$this->checkDataHistory($k,date('d M Y H:i:s',strtotime($v['date'])),'datetime');
	    	}

	    	if(isset($v['desc']))
	    	{
	    		$this->checkDataHistory($k,$v['desc'],'note');
	    	}	
	    	
	    }

		

   	}

    public function tracking_posindonesia($array=[]){
    	/*
	   	$res = $this->db_tbl->Client->request('GET', 'https://oms.8commerce.com/oms/index.php?r=posIndonesia/getTrackingStatus&awb_no='.$array['awb_no']);	
	   //	echo $res->getBody();die();
	   */	
	   	//echo $this->myUrl('https://oms.8commerce.com/oms/index.php?r=posIndonesia/getTrackingStatus&awb_no='.$array["awb_no"])->getAPI()->getBody();die();

	   	$body = json_decode($this->myUrl('https://oms.8commerce.com/oms/index.php?r=posIndonesia/getTrackingStatus&awb_no='.$array["awb_no"])->getAPI()->getBody(), TRUE)['rs_tnt'];

	   	if($body == ""){
	  	   die();
		}
		
		$end_arr = end($body['r_tnt']);
		$array_penerima = explode("PENERIMA", $end_arr['description']);
		
		if(isset($array_penerima[1]))
		{
			$this->receiver(str_replace(':','',str_replace($this->replace,"",$array_penerima[1])));
		}	

		if(isset($end_arr['eventDate']))
		{
			$this->date_received(date('d M Y H:i:s',strtotime($end_arr['eventDate'])));
		}

		if(isset($end_arr['office']))
		{
			$this->destination($end_arr['office']);
		}

		if(isset($end_arr['barcode']))
		{
			$this->service($end_arr['barcode']);
		}

		if(isset($end_arr['eventName']))
		{
				$this->status($end_arr['eventName']);
		}

   	}

   	public function trackingDetail_posindonesia(){

   		//echo $this->myUrl('https://oms.8commerce.com/oms/index.php?r=posIndonesia/getTrackingStatus&awb_no='.$this->arr[0]['awb_no'])->getAPI()->getBody();die();

	   	$body = json_decode($this->myUrl('https://oms.8commerce.com/oms/index.php?r=posIndonesia/getTrackingStatus&awb_no='.$this->arr[0]['awb_no'])->getAPI()->getBody(), TRUE)['rs_tnt'];

	   	if($body == ""){
	  	   die();
		}
		
		foreach($body['r_tnt'] as $k => $v){

	    	$this->defArrayDetailHistory($k);

	    	if(isset($v['eventDate']))
	    	{
	    		$this->checkDataHistory($k,date('d M Y H:i:s',strtotime($v['eventDate'])),'datetime');
	    	}

	    	if(isset($v['eventName']))
	    	{
	    		$this->checkDataHistory($k,$v['eventName'],'status');
	    	}

	    	if(isset($v['description']))
	    	{
	    		$this->checkDataHistory($k,$v['description'],'note');
	    	}	
	    	
	    }

   	}

   	public function tracking_ninja($array=[]){

		$body = json_decode($this->myUrl('https://api.ninjavan.co/id/dash/1.2/public/orders?tracking_id='.$array['awb_no'])->myUrlMethod('POST')->getAPI()->getBody(), TRUE);
	   	//echo $res->getBody();die();
	   	if(isset($body['error'])){
		   die();
		}

   	}

   	private function defArrayDetailHistory($no=0){

       $this->arrDetail[$no]['no'] =  $no+1;
       $this->arrDetail[$no]['datetime'] =  "-";
       $this->arrDetail[$no]['status'] =  "-";
       $this->arrDetail[$no]['note'] =  "-";

   	}

}