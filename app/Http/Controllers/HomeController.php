<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Response,View,Input,Auth,Session,Validator,File,Hash,DB,Mail,Storage;
use Illuminate\Support\Facades\Crypt;

use App\Models\ViewSummary;
use App\Models\ViewSummaryCompany;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function dashboard(Request $request){ 
        $perPage = $request->per_page;
        $date = $request->dateNow;
        $courier = $request->courier;

        if($request->date){
            $date = $request->date;
        }  

        $query = ViewSummary::where('date_invoice',$date)->orderBy('id','DESC');
        if ($courier) {
            $query = $query->where('courier_code', $courier);
        }

        return $query->paginate($perPage);
    }

    
    public function dashboardCompany(Request $request){ 
        $perPage = $request->per_page;
        $date = $request->dateNow;
        $courier = $request->courier; 
        $search = $request->filter;

        if($request->date){
            $date = $request->date;
        } 

        $query = ViewSummaryCompany::where('invoice_files_date_invoice',$date)->orderBy('id','DESC');
        if ($courier) { 
            $query = $query->where('couriers_courier_code', $courier);
        }
        if ($search) {
            $like = "%{$search}%";
            $query = $query->where('company_name', 'LIKE', $like);
			
        }
		//$get = $query->paginate($perPage);
		//dd($get);

        return $query->paginate($perPage);
    }


}