<?php
namespace App\Http\Controllers;
use App;
use Illuminate\Http\Request;
use Auth;

use Smalot\PdfParser\Parser;


class IndexController extends Controller
{
	
	public function __construct()
    {
		
	}
	
	public function index()
    { 
        if(Auth::check()){
            return redirect('home');
        }else{ 
            return view('site.login');
        }
    }
    
	public function forgotPassword()
    { 
        if(Auth::check()){
            return redirect('home');
        }else{ 
            return view('site.forgot-password');
        }
    }
    
    public function potongString()
    {  
        $str = '';
        
        $start = 'Kewarganegaraan';
        $end = 'Alamat'; 
        
        $pattern = sprintf(
            '/%s(.+?)%s/ims',
            preg_quote($start, '/'), preg_quote($end, '/')
        );
        
        if (preg_match($pattern, $str, $matches)) {
            list(, $match) = $matches;
            echo $match;
        }
    }

    
    private function getNama($string)
    {      
        $start = 'Nama :';
        $end = ';'; 
        
        $pattern = sprintf(
            '/%s(.+?)%s/ims',
            preg_quote($start, '/'), preg_quote($end, '/')
        );
        
        if (preg_match($pattern, $string, $matches)) {
            list(, $match) = $matches;
            return $match;
        }
    }

    
    private function getNamaAlias($string)
    {      
        $start = 'Nama alias :';
        $end = ';'; 
        
        $pattern = sprintf(
            '/%s(.+?)%s/ims',
            preg_quote($start, '/'), preg_quote($end, '/')
        );
        
        if (preg_match($pattern, $string, $matches)) {
            list(, $match) = $matches;
            return $match;
        }
    }
    
    private function arrayToTextAlias($array){
        $x = 1;
        foreach($array as $a){ 
            $res = $this->getNamaAlias($a); 
            if(!$res)continue;
            echo $x.'. Nama Alias: '.$res.'<br>';  
            $x++;
        }
    }

    
    private function arrayToTextNama($array){
        $x = 1;
        foreach($array as $a){
            $res = $this->getNama($a); 
            if(!$res)continue;  
            echo $x.'. Nama : '.$res.'<br>'; 
            $x++;
        }
    }

    public function extractPDF(Request $request)
    {     
        $path = public_path().'/invoice/book.pdf'; 
        $PDFParser = new Parser();
        $pdf = $PDFParser->parseFile($path);
        $pages  = $pdf->getPages();
        $totalPages = count($pages);
        $currentPage = 1;
        $text = ""; 
        $jml = 100;
        foreach ($pages as $page) { 
            $text .= $page->getText(); 
            $currentPage++;
        }

        $str = preg_replace('/(\v|\s)+/', ' ', $text);
        $add = str_replace('KEPOLISIAN NEGARA REPUBLIK INDONESIA MARKAS BESAR Jalan Trunojoyo 3, Kebayoran Baru, Jakarta 12110 “Pro Justitia ” DAFTAR TERDUGA TERORIS DAN ORGANISASI TERORIS', '', $str);
        $add = str_replace('I. INDIVIDU: ', '', $add);
        $add = str_replace('II. ENTITAS: ', '', $add);
        $variable = substr($add, 0, strpos($add, "III. KETERANGAN"));
        $start = 'Nomor:';
        $end = '1. Nama'; 
        $pattern = sprintf(
                '/%s(.+?)%s/ims',
                preg_quote($start, '/'), preg_quote($end, '/')
        ); 
        if (preg_match($pattern, $variable, $matches)) {
                list(, $match) = $matches;
                $concat = $start.$match;
                $nextString = str_replace($concat, "", $variable);   
               // echo $match; //get document number  
                echo $nextString;
            } 
       
    }
	
	public function pageNotFound()
    {   
        return view('site.error-404'); 
    }

    
	public function serverError()
    {   
        return view('site.error-500'); 
    }
    
	    public function lang($lang)
    {
        App::setLocale($lang);
        session()->put('locale', $lang);
        return redirect()->back();
    }

    
    public function extractPDF2(Request $request)
    {     
        $path = public_path().'/invoice/book.pdf'; 
        $PDFParser = new Parser();
        $pdf = $PDFParser->parseFile($path);
        $pages  = $pdf->getPages();
        $totalPages = count($pages);
        $currentPage = 1;
        $text = ""; 
        $jml = 100;
        foreach ($pages as $page) { 
            $text .= $page->getText(); 
            $currentPage++;
        }

        $str = preg_replace('/(\v|\s)+/', ' ', $text);
        $add = str_replace('KEPOLISIAN NEGARA REPUBLIK INDONESIA MARKAS BESAR Jalan Trunojoyo 3, Kebayoran Baru, Jakarta 12110 “Pro Justitia ” DAFTAR TERDUGA TERORIS DAN ORGANISASI TERORIS', '', $str);
        $add = str_replace('I. INDIVIDU: ', '', $add);
        $add = str_replace('II. ENTITAS: ', '', $add);
        $variable = substr($add, 0, strpos($add, "III. KETERANGAN"));
        $start = 'Nomor:';
        $end = '1. Nama'; 
        $pattern = sprintf(
                '/%s(.+?)%s/ims',
                preg_quote($start, '/'), preg_quote($end, '/')
        ); 
        if (preg_match($pattern, $variable, $matches)) {
                list(, $match) = $matches;
                $concat = $start.$match;
                $nextString = str_replace($concat, "", $variable);   
               // echo $match; //get document number  
               $this->getByNumber($nextString,1);
            } 
    }


    private function getByNumber($string,$no)
    {
        $jml= $no+1;
        for($no;$no<$jml;$no++){ 
            $start = ' '.$no.'.';
            $end = ' '.$jml.'.'; 
            $pattern = sprintf(
                '/%s(.+?)%s/ims',
                preg_quote($start, '/'), preg_quote($end, '/')
            );
            $rmv = $start.$end;
            if (preg_match($pattern, $string, $matches)) {
                list(, $match) = $matches;
                $concat = $start.$match;
                $nextString = str_replace($concat, "", $string); 
                $this->getByNumber($nextString,$jml);  
                // echo $match.'<br><br><br>'; 
                $explode = explode(':', $match );
                $this->extractArray($explode);
                echo '<br><br><br>'; 
            }
        }
    }

    
    public function extractArray($array)
    {
        
    }

    

}