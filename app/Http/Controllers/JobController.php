<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Response,View,Input,Session,Validator,File,Hash,DB,Mail;
use Illuminate\Support\Facades\Crypt;

use App\Models\LogActivity;
use App\Models\Invoice;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use App\Jobs\ExtractFiles;

class JobController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         
    }

    public static function extractFiles($data){ 
        $cek = Invoice::findOrFail($data['id']);
        $ExtractFiles = (new ExtractFiles($cek))->delay(Carbon::now()->addMinutes(1));
        dispatch($ExtractFiles); 
        $cek->update(array('status' => 'processing'));   
        return response()->json(['status'=>200,'data'=>'','message'=>'Process Successfully']);   
    }
    

    

    public function extractFilesData(Request $request){ 
          
        $path = public_path().'/invoice/jne.xlsx'; 
        $rows = [];
        $courier_code = $request->courier_code;;


        if($courier_code == 'tiki'){
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
            $worksheet = $spreadsheet->setActiveSheetIndex(0)->rangeToArray('A1:M1000'); 
            foreach ($worksheet AS $row) {
                if($row[0] == "No") continue;
                
                $cek = $this->checkNumber($row[0]);
                if($cek == true){
                    $rows[] = $row; 
                }
            } 
        }elseif($courier_code == 'adex'){
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
            $worksheet = $spreadsheet->setActiveSheetIndex(0)->rangeToArray('A1:R1000'); 
            foreach ($worksheet AS $row) {
                if($row[0] == "No") continue;
                
                $cek = $this->checkNumber($row[0]);
                if($cek == true){
                    $rows[] = $row; 
                }
            } 

        }elseif($courier_code == 'jne'){
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
            $worksheet = $spreadsheet->setActiveSheetIndex(0)->rangeToArray('A1:L1000'); 
            foreach ($worksheet AS $row) { 
                $cek = $this->checkNumber($row[0]);
                if($cek == true){
                    $rows[] = $row; 
                }
            } 

        }else{
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
            $worksheet = $spreadsheet->getActiveSheet();
            foreach ($worksheet->getRowIterator() AS $row) {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(TRUE); // This loops through all cells,
                $cells = [];
                foreach ($cellIterator as $cell) {
                    $cells[] = $cell->getValue();
                }
                $rows[] = $cells;
            }

        }
        //return response()->json($rows);
        return $this->checkCourier($courier_code,$rows);
    }

    private function checkCourier($courier,$array){
 
        if($courier == 'sap'){
            $this->courierSAP($array); 
        }elseif($courier == 'j&t'){
            $this->courierJET($array); 
        }elseif($courier == 'tiki'){
            $this->courierTIKI($array); 
        }elseif($courier == 'wahana'){
            return $this->courierWAHANA($array); 
        }elseif($courier == 'adex'){
            return $this->courierADEX($array); 
        }elseif($courier == 'jne'){
            return $this->courierJNE($array); 
        }else{
            Invoice::where('id',$this->invoice->id)->update(array('status' => 'error', 'remarks' => 'courier code not found in backend', 'data' => json_encode($array)));
        } 

    }

    private function courierJNE($array){
        ini_set('max_execution_time', 0); // 0 = Unlimited
        $x=0;
        $res = [];
        for($x; $x < count($array);$x++){ 
            $res[] = ['invoice_files_id'=>'', 'customer_code' => '', 'status' => 'true', 'service' =>$array[$x][11], 'awb_no' => $array[$x][2] ,'qty' => $array[$x][5], 'weight_total' => $array[$x][6], 'bag_total' => 0, 'cod_amount' => 0, 'insurance_amount' => $this->rupiahToInt($array[$x][8]) , 'weight_price' => 0, 'other_amount' => $this->rupiahToInt($array[$x][9]),'price' =>$this->rupiahToInt($array[$x][7])];             
        } 

        return response()->json($res); 

    }


    private function courierADEX($array){
        ini_set('max_execution_time', 0); // 0 = Unlimited
        $x=0;
        $res = [];
        for($x; $x < count($array);$x++){ 
            $res[] = ['invoice_files_id'=>'', 'customer_code' => '', 'status' => 'true', 'service' =>'', 'awb_no' => $array[$x][2] ,'qty' => 0, 'weight_total' => $array[$x][9], 'bag_total' => $array[$x][8], 'cod_amount' => 0, 'insurance_amount' => 0 , 'weight_price' => $this->rupiahToInt($array[$x][14]), 'other_amount' => $this->rupiahToInt($array[$x][15]),'price' => $this->rupiahToInt($array[$x][16])];             
        } 

        return response()->json($res); 

    }

    private function courierWAHANA($array){
        ini_set('max_execution_time', 0); // 0 = Unlimited
        $x=2;
        $res = [];
        for($x; $x < count($array);$x++){

            $res[] = ['invoice_files_id'=>'', 'customer_code' => $array[$x][3], 'status' => 'true', 'service' =>$array[$x][11], 'awb_no' => $array[$x][2] ,'qty' => 0, 'weight_total' => $array[$x][9], 'bag_total' => $array[$x][8], 'cod_amount' => 0, 'insurance_amount' => 0 , 'weight_price' => $array[$x][15] ,'price' => $array[$x][16]];             
        } 

        return response()->json($res); 

    }


    private function rupiahToInt($value){
        return preg_replace("/[^0-9]/", "", $value);
    }

 

    
    private function otherAmount($courier,$array){
        if($courier == 'sap'){
            $pulus = $this->IsNullOrEmptyString($array[44]) + $this->IsNullOrEmptyString($array[46]) + $this->IsNullOrEmptyString($array[47]);
            return $pulus;
        }

        return false;
    }

    private function checkNumber($no) {  
      if(is_numeric($no)){
         return true;
        }else{
        return false;
        }
    }

    private function IsNullOrEmptyString($str){
        if(!isset($str) || trim($str) === ''){
            return 0;
        }else{
            return $str;
        }
    }


}