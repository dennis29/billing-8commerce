import VueRouter from 'vue-router';
import Home from './components/Site/Index.vue';
import ServerError from './components/Site/ServerError.vue';
import PageNotFound from './components/Site/PageNotFound.vue';
import Profile from './components/Users/Profile.vue';
import UserPassword from './components/Users/UserPassword.vue';
import Users from './components/Users/Index.vue';
import UserAdd from './components/Users/Add.vue';
import UserEdit from './components/Users/Edit.vue';
import UserDetail from './components/Users/Detail.vue';
import Roles from './components/Roles/Index.vue';
import Couriers from './components/Couriers/Index.vue';
import Customers from './components/Customers/Index.vue';
import Area from './components/Area/Index.vue';
import PriceForPublish from './components/PriceForPublish/Index.vue';
import PriceFor8Commerce from './components/PriceFor8Commerce/Index.vue';
import PriceForCustomer from './components/PriceForCustomer/Index.vue';
import MultiDestPriceFor8com from './components/MultiDestPriceFor8com/Index.vue';
import MultiDestPriceForCus from './components/MultiDestPriceForCus/Index.vue';
import GeneralDiscount from './components/GeneralDiscount/Index.vue';
import RangeDiscount from './components/RangeDiscount/Index.vue';
import CustomerDiscount from './components/CustomerDiscount/Index.vue';
import Invoice from './components/Invoice/Index.vue';
import InvoiceAdd from './components/Invoice/Add.vue';
import InvoiceDetail from './components/Invoice/Detail.vue';
import InvoiceDetailByCompany from './components/Invoice/DetailByCompany.vue';
import InvoiceEdit from './components/Invoice/Edit.vue';
import InvoiceTemplete from './components/InvoiceTemplete/Index.vue';
import CekResi from './components/CekResi/Index.vue';
import {Money} from 'v-money'

let routes=[
{
	path:'/',
    name: 'home',
    component: Home
}, 
/** Users */
{
	path:'/profile',
    name: 'profile',
    component: Profile
}, 
{
	path:'/change-password',
    name: 'userpassword',
    component: UserPassword
}, 
{
	path:'/list-users',
    name: 'users',
    component: Users
}, 
{
	path:'/user-add',
	name: 'useradd',
	component:UserAdd,
	props: true
},
{
	path:'/user-edit/:id',
	name: 'useredit',
	component:UserEdit,
	 props: true
},
{
	path:'/user-detail/:id',
    name: 'userdetail',
    component: UserDetail,
	props: true
}, 

/** Roles */
{
	path:'/list-roles',
    name: 'roles',
    component: Roles
}, 

/** Courier */
{
	path:'/list-couriers',
    name: 'couriers',
    component: Couriers
}, 

/** Customer */
{
	path:'/list-customers',
    name: 'customers',
    component: Customers
}, 

/** Invoice templete */
{
	path:'/list-invoice-templete',
    name: 'invoicetemplete',
    component: InvoiceTemplete
}, 
/** Invoice templete */

/** Invoice */
{
	path:'/list-invoice',
    name: 'invoice',
    component: Invoice
}, 
{
	path:'/invoice-add',
	name: 'invoiceadd',
	component:InvoiceAdd,
	props: true
},
{
	path:'/invoice-detail/:id',
	name: 'invoicedetail',
	component:InvoiceDetail
},
{
	path:'/invoice-edit/:id',
	name: 'invoiceedit',
	component:InvoiceEdit,
	props: true
},
{
	path:'/invoice-detail-by-company/:id/:company',
	name: 'invoicedetailbycompany',
	component:InvoiceDetailByCompany
},

{
	path:'/cek-resi',
    name: 'cekResi',
    component: CekResi
},

/**
 * Error
*/
{
	path:'/server-error',
    name: 'servererror',
    component: ServerError
}, 
{
	path:'*', 
    component: PageNotFound
}, 
{
	path:'/page-not-found',
    name: 'pagenotfound',
    component: PageNotFound
}, 
{
	path:'/list-area',
    name: 'area',
    component: Area
},
{
	path:'/list-priceForPublish',
    name: 'priceForPublish',
    component: PriceForPublish
},
{
	path:'/list-priceFor8Commerce',
    name: 'priceFor8Commerce',
    component: PriceFor8Commerce
},
{
	path:'/list-multiDestPriceFor8com',
    name: 'multiDestPriceFor8com',
    component: MultiDestPriceFor8com
},
{
	path:'/list-priceForCustomer',
    name: 'priceForCustomer',
    component: PriceForCustomer
},
{
	path:'/list-multiDestPriceForCus',
    name: 'multiDestPriceForCus',
    component: MultiDestPriceForCus
},
{
	path:'/list-generalDiscount',
    name: 'generalDiscount',
    component: GeneralDiscount
},
{
	path:'/list-rangeDiscount',
    name: 'rangeDiscount',
    component: RangeDiscount
},
{
	path:'/list-customerDiscount',
    name: 'customerDiscount',
    component: CustomerDiscount
}

];

export default new VueRouter({
	routes,
	linkActiveClass: 'active',
	components: {Money},
});