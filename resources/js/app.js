import router from './routes.js';
import JQuery from 'jquery'

require('./bootstrap');
require('chart.js');
require('es6-promise').polyfill();

axios.interceptors.response.use(null, function(error) {
  if (error.status === 401) {
    return defaultResponse;
  }
  return Promise.reject(error);
});

Vue.mixin({

      data: function() {

        return {

          myGlobalVar:'this is my ItSolutionStuff.com'

        }

      },
	  methods:{

        checkAngka(evt){
			  evt = (evt) ? evt : window.event;
			  var charCode = (evt.which) ? evt.which : evt.keyCode;
			  if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
				evt.preventDefault();;
			  } else {
				return true;
			  }
	    },
		
		customFormatter(date) {
                return moment(date).format('YYYY-MM-DD');
        },
		
		customFormatterMonth(date) {
                return moment(date, 'MM/YYYY').format('MMM-YYYY');
        },
		
		formatDate (value, fmt = 'YYYY-MM') {
                return (value == null)
                    ? ''
                    : moment(value, 'YYYY-MM').format(fmt)
        },
		
		dataService () {
                this.loading();
                axios.get('/getService').then((response) => {
                    if(!response.data){ 
                        window.location.href = window.webURL; 
                    }else{ 
                        if(response.data.status == 200){ 
                            this.services = response.data.data;
                        }else{ 
                            this.resultError(response.data.message) 
                        }
                    }
                }).catch(error => {
                    if (! _.isEmpty(error.response)) {
                        if (error.response.status = 422) {
                            this.resultError(error.response.data.errors) 
                        }else if (error.response.status = 500) {
                            this.$router.push('/server-error');
                        }else{
                            this.$router.push('/page-not-found');
                        }
                    }
                });
        },
		
		insertAreaId (group) {
			console.log(group.selectedObject.id);
			this.forms.area_id = group.selectedObject.id;
			// access the autocomplete component methods from the parent
			//this.$refs.autocomplete.clear()
		},
		
		customFormatter(date) {
                return moment(date).format('YYYY-MM');
        },

		
		customFormatterMonthYM(date) {
			if(date)
			{
                return moment(date, 'YYYY-MM').format('MMM-YYYY');
			}	
        },
		
		customFormatterYearMonthDate(date) {
                return moment(date, 'MM/YYYY').format('MMM-YYYY');
        },
		
		customFormatterMY(date) {
                return moment(date).format('MM/YYYY');
        },

        customFormatterMYX(date) {
                  return moment(date, 'MM/YYYY').format('MMM-YYYY');
        },
					
		formatPriceWithCondition(value) {
				if(!value.includes('%')){
					var formatter = new Intl.NumberFormat('id', {
					  style: 'currency',
					  currency: 'IDR',
					});
	 
					return Math.floor(value).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
				}
				
				return value;
	
		},
		
		customFormatterYM(date) {
			var ym = "";
			if(date)
			{
                ym =  moment(date).format('YYYY-MM');
			}
			return ym;
			
			
        },
					
		formatPrice(value) {
		        var formatter = new Intl.NumberFormat('id', {
				  style: 'currency',
				  currency: 'IDR',
				});
				if(value != null)
					return Math.floor(value).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
				else
					return "null";
			},

      }

})

const app = new Vue({
    el: '#app',
    router
});

