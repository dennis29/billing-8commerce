<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceFileDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_file_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('invoice_files_id')->nullable();
            $table->string('customer_code')->nullable();
            $table->enum('status', ['true', 'false'])->default('true'); 
            $table->string('service')->nullable();
            $table->string('awb_no')->nullable();
            $table->integer('qty')->default(1);
            $table->double('weight_total',8,2)->default(0);
            $table->double('bag_total',8,2)->default(0);
            $table->double('cod_amount')->default(0);
            $table->double('insurance_amount')->default(0);
            $table->double('weight_price')->default(0);
            $table->double('price')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_file_details');
    }
}
