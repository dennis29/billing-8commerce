<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountByRangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_by_ranges', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->integer('courier_id');
			$table->string('contract_no');
			$table->integer('min_value');
			$table->integer('max_value');
			$table->integer('discount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_by_ranges');
    }
}
