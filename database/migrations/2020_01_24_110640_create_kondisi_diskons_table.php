<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKondisiDiskonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kondisi_diskons', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->integer('courier_id');
			$table->string('contract_no');
			$table->integer('diskon_8comm')->nullable();
			$table->integer('start_month')->nullable();
			$table->integer('end_month')->nullable();
		    $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kondisi_diskons');
    }
}
