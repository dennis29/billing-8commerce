<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_areas', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->integer('kurir');
			$table->string('asal_kota');
			$table->string('asal_negara');
			$table->string('asal_zip_code');
			$table->string('tujuan_provinsi');
			$table->string('tujuan_kota');
			$table->string('tujuan_kecamatan');
			$table->string('tujuan_kelurahan');
			$table->string('tujuan_zip_code');
			$table->string('tujuan_zona');
			$table->string('cod');
			$table->string('eta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_areas');
    }
}
