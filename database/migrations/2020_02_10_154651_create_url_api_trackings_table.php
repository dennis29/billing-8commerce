<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUrlApiTrackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('url_api_trackings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->string('url');
            $table->string('courier_id');
            $table->string('form_params');
            $table->string('headers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('url_api_trackings');
    }
}
