<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) { 
            $table->bigIncrements('id');
            $table->integer('invoice_files_id');
            $table->integer('order_id'); 
            $table->integer('order_detail_id'); 
            $table->integer('order_header_id');  
            $table->string('sku_code')->nullable();
            $table->string('sku_description')->nullable();
            $table->integer('qty_order')->default(0);
            $table->double('price')->default(0);
            $table->double('amount_order')->default(0);
            $table->integer('qty_ship')->default(0);
            $table->double('amount_ship')->default(0);
            $table->string('remarks')->nullable();
            $table->string('status')->nullable();
            $table->dateTime('create_time')->nullable();
            $table->dateTime('update_time')->nullable();
            $table->integer('insured')->default(0);
            $table->string('sku_parent')->nullable();
            $table->double('order_price')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
