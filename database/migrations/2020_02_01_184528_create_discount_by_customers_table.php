<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountByCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_by_customers', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->integer('courier_id');
			$table->integer('customer_id');
			$table->string('contract_no');
			$table->integer('discount');
			$table->string('start_month_condition')->nullable();
			$table->string('end_month_condition')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_by_customers');
    }
}
