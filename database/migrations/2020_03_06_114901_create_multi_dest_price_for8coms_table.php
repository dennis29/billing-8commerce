<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMultiDestPriceFor8comsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multi_dest_price_for8coms', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->integer('area_id');
			$table->string('contract_no');
			$table->string('service');
			$table->integer('harga_publish');
			$table->integer('start_month')->nullable();
			$table->integer('end_month')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multi_dest_price_for8coms');
    }
}
