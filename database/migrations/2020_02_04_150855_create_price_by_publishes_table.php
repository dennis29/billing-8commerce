<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceByPublishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_by_publishes', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->integer('area_id');
			$table->string('contract_no');
			$table->string('service');
			$table->integer('harga_publish');
			$table->integer('eta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_by_publishes');
    }
}
