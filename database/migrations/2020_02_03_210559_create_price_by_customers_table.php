<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceByCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_by_customers', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->integer('area_id');
			$table->string('contract_no');
			$table->integer('customer_id');
			$table->string('service');
			$table->integer('harga_publish');
			$table->integer('start_month')->nullable();
			$table->integer('end_month')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_by_customers');
    }
}
