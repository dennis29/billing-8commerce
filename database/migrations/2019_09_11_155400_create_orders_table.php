<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('invoice_files_id');
            $table->integer('invoice_file_details_id');
            $table->integer('order_header_id');            
            $table->string('company_id')->nullable();
            $table->string('company_name')->nullable();
            $table->string('order_no')->nullable();
            $table->date('order_date')->nullable();
            $table->date('due_date')->nullable();
            $table->string('status')->nullable();
            $table->string('courier_id')->nullable();
            $table->string('awb_no')->nullable();
            $table->integer('insured')->nullable(0);
            $table->string('insured_by_id')->nullable();
            $table->integer('cod')->nullable(0);
            $table->string('payment_status')->nullable();
            $table->string('create_by')->nullable();
            $table->string('dest_name')->nullable();
            $table->longText('dest_address1')->nullable();
            $table->longText('dest_address2')->nullable();
            $table->string('dest_province')->nullable();
            $table->string('dest_city')->nullable();
            $table->string('dest_area')->nullable();
            $table->string('dest_sub_area')->nullable();
            $table->string('dest_postal_code')->nullable();
            $table->string('dest_village')->nullable();
            $table->text('dest_remarks')->nullable();
            $table->string('ori_name')->nullable();
            $table->text('ori_address1')->nullable();
            $table->text('ori_address2')->nullable();
            $table->string('ori_province')->nullable();
            $table->string('ori_city')->nullable();
            $table->string('ori_area')->nullable();
            $table->string('ori_sub_area')->nullable();
            $table->string('ori_postal_code')->nullable();
            $table->string('ori_village')->nullable();
            $table->longText('ori_remarks')->nullable();
            $table->string('fulfillment_center_id')->nullable();
            $table->string('order_source')->nullable();
            $table->dateTime('create_time')->nullable();
            $table->dateTime('update_time')->nullable();
            $table->string('dest_phone')->nullable();
            $table->string('dest_mobile')->nullable();
            $table->string('dest_phone2')->nullable();
            $table->string('dest_email')->nullable();
            $table->string('ori_country')->nullable();
            $table->string('dest_country')->nullable();
            $table->string('promo_code')->nullable();
            $table->string('special_packaging')->nullable();
            $table->double('order_amount')->nullable(0);
            $table->double('shipping_amount')->nullable(0);
            $table->double('insurance_amount')->nullable(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
